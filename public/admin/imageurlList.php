<?php
require_once("../../php/Admin.php");

set_time_limit(0);
$imageList = $admin->imageList();
$total = 0;
for($i=0,$max=count($imageList);$i<$max;$i++){
	if(empty($imageList[$i]['image_url'])) continue;
	$binary = file_get_contents($imageList[$i]['image_url']);
	$kSize = strlen($binary)/1024;
	print (int)$kSize. "KB    ";
	print $imageList[$i]['image_url']. "<br />";

	$total += $kSize;
}

print "<h2>total: ". (int)$total. " KB</h2>";