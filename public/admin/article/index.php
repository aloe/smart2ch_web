<?php
require_once("../../../php/Admin.php");
$list = $admin->articleList();
// pr($list);
?>

<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
<title>記事</title>
<link rel="stylesheet" href="/admin/style.css" media="all">
</head>
<body>
	<div id="contents">
		<ul>
			<li><a href="/admin/site">サイト</a></li>
			<li><a href="/admin/article">記事</a></li>
		</ul>
		<h2>サイト</h2>
		<table>
			<tr>
				<th>id</th><th>サイト名</th><th>内容</th><th>公開日</th>
			</tr>
			<?php for($i=0,$max=count($list);$i<$max;$i++): $row=$list[$i]; ?>
				<tr>
					<td><?php print $row['id']; ?></td>
					<td><?php print $row['site']['name']; ?></td>
					<td>
						<?php if(!empty($row['image_url'])): ?>
							<div style="float:left;margin-right:10px;"><img src="<?php print $row['image_url']; ?>" width="100" /></div>
						<?php endif; ?>
						<div>
							<a href="<?php print $row['link_url']; ?>" target="_blank"><?php print $row['title']; ?></a>
							<hr />
							<?php print $row['description']; ?>
						</div>
					</td>
					<td><?php print date('Y-m-d H:i:s', $row['published_at']); ?></td>
				</tr>
			<?php endfor; ?>
		</table>
	</div>
</div>
