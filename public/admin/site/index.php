<?php
require_once("../../../php/Admin.php");
$admin->addSite();
$admin->deleteSite();
$list = $admin->siteList();
?>

<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
<title>サイト</title>
<link rel="stylesheet" href="/admin/style.css" media="all">
</head>
<body>
	<div id="contents">
		<ul>
			<li><a href="/admin/site">サイト</a></li>
			<li><a href="/admin/article">記事</a></li>
		</ul>
		<h2>サイト</h2>
		<form method="POST">
			<table>
				<tr>
					<th>サイト名</th>
					<td><input type="text" name="name" /></td>
				</tr>
				<tr>
					<th>url</th>
					<td><input type="text" name="url" /></td>
				</tr>
				<tr>
					<th>RSS url</th>
					<td><input type="text" name="rss_url" /></td>
				</tr>
				<tr>
					<td colspan="2"><input type="submit" name="submit" value="登録" /></td>
				</tr>
			</table>
		</form>
		<h1>登録済み</h1>
		<table>
			<tr>
				<th>id</th><th>サイト名</th><th>RSS</th><th>apple</th><th>編集</th><th>削除</th>
			</tr>
			<?php for($i=0,$max=count($list);$i<$max;$i++): $site=$list[$i]; ?>
				<tr>
					<td><?php print $site['id']; ?></td>
					<td><a href="<?php print $site['url']; ?>" target="_blank">
						<?php print $site['name']; ?>
						</a></td>
					<td><a href="<?php print $site['rss_url']; ?>" target="_blank">
						<?php print $site['rss_url']; ?>
					</a></td>
					<td><?php print ($site['use_review']) ? "○" : "-"; ?></td>
					<td><a href="edit.php?id=<?php print $site['id']; ?>">編集</a></td>
					<td><a href="?deleteId=<?php print $site['id']; ?>"
						onclick="return confirm('削除します');"
						>削除</a></td>
				</tr>
			<?php endfor; ?>
		</table>
	</div>
</body>

</html>