<?php
require_once("../../../php/Admin.php");
$admin->editSite();
$site = $admin->site((int)$_GET['id']);
?>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
<title>サイト編集</title>
<link rel="stylesheet" href="/admin/style.css" media="all">
</head>
<body>
	<p><a href="/admin/site">＜一覧へ</a></p>
	<div id="contents">
		<h2>サイト編集</h2>
		<form method="POST">
			<table>
				<tr>
					<th>サイト名</th>
					<td><input type="text" name="name" value="<?php print $site['name']; ?>" /></td>
				</tr>
				<tr>
					<th>url</th>
					<td><input type="text" name="url" value="<?php print $site['url']; ?>" /></td>
				</tr>
				<tr>
					<th>RSS url</th>
					<td><input type="text" name="rss_url" value="<?php print $site['rss_url']; ?>" /></td>
				</tr>
				<tr>
					<td colspan="2">
						<input type="submit" name="submit" value="登録" />
						<input type="hidden" name="id" value="<?php print $site['id']; ?>" />
					</td>
				</tr>
			</table>
		</form>
	</div>
</body>
</html>