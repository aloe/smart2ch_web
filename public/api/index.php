<?php
require_once("../../php/Api.php");
$data = $api->apiData();
$json = json_encode($data);
header( 'Content-Type: text/javascript; charset=utf-8' );
header("Content-Length: ". mb_strlen($json));
print $json;