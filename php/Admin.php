<?php

require_once("config.php");
require_once("Base.php");

class Admin extends Base{

	private $siteList;

	public function addSite(){
		if(empty($_POST['submit'])) return;

		$param = $_POST;
		$sql = "INSERT INTO site (name, url, rss_url, created_at) "
		." VALUES "
		." (:name, :url, :rss_url, :created_at)";
		$stm = $this->prepare($sql);
		$stm->bindValue(':name', $param['name']);
		$stm->bindValue(':url', $param['url']);
		$stm->bindValue(':rss_url', $param['rss_url']);
		$stm->bindValue(':created_at', time());
		$stm->execute();

	}

	public function editSite(){
		if(empty($_POST['submit'])) return;


		$param = $_POST;
		$sql = "UPDATE site SET name = :name, url = :url, rss_url = :rss_url WHERE id = :id";
		$stm = $this->prepare($sql);
		$stm->bindValue(':name', $param['name']);
		$stm->bindValue(':url', $param['url']);
		$stm->bindValue(':rss_url', $param['rss_url']);
		$stm->bindValue(':id', $param['id']);
		$stm->execute();

		header('Location: /admin/site');
	}

	public function site($id){
		$sql = sprintf("SELECT * FROM site WHERE id = %d", $id);
		return $this->getSqlResultRow($sql);
	}

	public function siteList(){
		$sql = "SELECT * FROM site ORDER BY id DESC";
		$list = $this->getSqlResult($sql);

		return $list;
	}

	public function deleteSite(){
		if(empty($_GET['deleteId'])) return;

		$deleteId = (int) $_GET['deleteId'];
		$sql = sprintf("DELETE FROM site WHERE id = %d", $deleteId);
		$this->query($sql);
	}

	public function articleList(){
		$this->siteList = $this->siteList();
		$sql = "SELECT * FROM article ORDER BY published_at DESC LIMIT 100";
		$articleList = $this->getSqlResult($sql);
		$this->mergeSite($articleList);

		return $articleList;
	}

	private function mergeSite(&$articleList){
		for($i=0,$max=count($articleList);$i<$max;$i++){
			$this->mergeSiteRow($articleList[$i]);
		}
	}

	private function mergeSiteRow(&$article){
		$siteList = $this->siteList;
		for($i=0,$max=count($siteList);$i<$max;$i++){
			$row = $siteList[$i];
			if($article['site_id'] == $row['id']){
				$article['site'] = $row;
				return;
			}
		}
	}

	public function saveJson(){
		$apiData = $this->apiData();
		$str = json_encode($apiData);
		file_put_contents(API_JSON_FILE_PATH, $str);
	}

	private function apiData(){
		$sql = "SELECT * FROM site ORDER BY priority DESC";
		$siteList = $this->getSqlResult($sql);

		$sql = "SELECT id, site_id, title, link_url, image_url FROM article ORDER BY published_at DESC LIMIT 7000";
		$articleList = $this->getSqlResult($sql);

		return array(
			'site'=>$siteList
			, 'article'=>$articleList
		);
	}

}

$admin = new Admin(DB_HOST, DB_NAME, DB_USER, DB_PASSWORD);

