<?php

abstract class Base{

	private $pdo;

	function __construct($dbHost, $dbName, $dbUser, $dbPassword){
		$query = sprintf("mysql:host=%s;dbname=%s", $dbHost, $dbName);
		// new PDO();
		$this->pdo = new PDO($query, $dbUser, $dbPassword, array(
			PDO::MYSQL_ATTR_INIT_COMMAND => "SET CHARACTER SET 'utf8'"
			, PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
			, PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
		));

	}

	protected function getSqlResult($sql){
		$result = $this->pdo->query($sql);
		$data = $result->fetchAll();
		return $data;
	}

	protected function getSqlResultRow($sql){
		$result = $this->pdo->query($sql);
		$data = $result->fetch();
		return $data;
	}

	protected function getList($tableName){
		$sql = sprintf("SELECT * FROM %s ORDER BY created_at DESC", $tableName);
		return $this->getSqlResult($sql);
	}

	protected function prepare($sql){
		return $this->pdo->prepare($sql);
	}

	protected function query($sql){
		return $this->pdo->query($sql);
	}

	protected function lastInsertId(){
		return $this->pdo->lastInsertId();
	}

}