<?php

ini_set("display_errors", 1);

require_once("config.php");
require_once("Base.php");
require_once("simple_html_dom.php");

class Parser extends Base{

	public function parse(){
		set_time_limit(0);
		$siteList = $this->siteList();

		for($i=0,$max=count($siteList);$i<$max;$i++){
			$row = $siteList[$i];
			try{
				$this->parseRow($row['id'], $row['rss_url']);
			}catch (Exception $e) {
				print $e->getMessage();
			}
		}
	}

	const SITE_ID_ITSOKUHOU = 94;
	const SITE_ID_OMOKORO = 93;
	const SITE_ID_LIFEHACKER = 92;
	const SITE_ID_CNET = 91;
	const SITE_ID_GIGAZINE = 90;
	const SITE_ID_GIZMODE = 89;
	const SITE_ID_BYOKAN = 88;
	const SITE_ID_WARANOTE = 87;
	const SITE_ID_RAJIKKU = 86;
	const SITE_ID_NISIKI = 85;
	const SITE_ID_YASESOKU = 84;
	const SITE_ID_MOMIAGE = 83;
	const SITE_ID_MOEOTA = 82;
	const SITE_ID_MESIUMA = 81;
	const SITE_ID_MUDAIDOCUMENT = 80;
	const SITE_ID_MATOMEBIJON = 79;
	const SITE_ID_CHANNELPLUS = 78;
	const SITE_ID_MATOMESU = 77;
	const SITE_ID_MATOMEIDE = 76;
	const SITE_ID_BURUSOKU = 75;
	const SITE_ID_PURISOKU = 74;
	const SITE_ID_BURABURA = 73;
	const SITE_ID_FRIDECHIKIN = 72;
	const SITE_ID_FESOKU = 71;
	const SITE_ID_BILOG = 70;
	const SITE_ID_HIROIMONOTYUDOKU = 69;
	const SITE_ID_HIMAJIN = 68;
	const SITE_ID_HAMSTER = 67;
	const SITE_ID_HACHIMA = 66;
	const SITE_ID_VIPSYOKUNIN = 65;
	const SITE_ID_HART = 64;
	const SITE_ID_NENSYU = 63;
	const SITE_ID_NETA = 62;
	const SITE_ID_NEWS30OVER = 61;
	const SITE_ID_NW2 = 60;
	const SITE_ID_EVESOKU = 59;
	const SITE_ID_NIKONIKO = 58;
	const SITE_ID_TOKIHAKITA = 57;
	const SITE_ID_TETUGAKU = 56;
	const SITE_ID_DEJITALNEWS = 55;
	const SITE_ID_DIETSOKUHOU = 54;
	const SITE_ID_SEIYUSOKUHOU = 53;
	const SITE_ID_SYURABA = 52;
	const SITE_ID_KABUZENRYOKU = 51;
	const SITE_ID_KOPIPEJOHOKYOKU = 49;
	const SITE_ID_GOLDENTIMES = 48;
	const SITE_ID_GEINOU2CH = 47;
	const SITE_ID_GEINOUNEWS = 46;
	const SITE_ID_GURASOKU = 44; 
	const SITE_ID_KINISOKU = 43;
	const SITE_ID_KARUKAN = 42;
	const SITE_ID_KANASOKU = 41;
	const SITE_ID_KAOSU = 40;
	const SITE_ID_ORYOURI = 39;
	const SITE_ID_JIN = 37;
	const SITE_ID_OGA = 36;
	const SITE_ID_ONIYOME = 35;
	const SITE_ID_ENSOKU = 33;
	const SITE_ID_UWAKI = 32;
	const SITE_ID_INSYOKU = 31;
	const SITE_ID_IMOUTOHAVIPPER = 30;
	const SITE_ID_ITASIN = 29;
	const SITE_ID_ITAI = 28;
	const SITE_ID_ANISOKUMAX = 25;
	const SITE_ID_AJAJA = 24;
	const SITE_ID_WHATS = 23;
	const SITE_ID_VIPWIDE = 22;
	const SITE_ID_VIPORE = 21;
	const SITE_ID_VIPSOKUHOU = 20;
	const SITE_ID_UPLOAD = 19;
	const SITE_ID_TEMITA = 18;
	const SITE_ID_POKETTI = 17;
	const SITE_ID_OTODASU = 16;
	const SITE_ID_ASOBU = 14;
	const SITE_ID_FX2CH = 13;
	const SITE_ID_FOOTBALLNET = 12;
	const SITE_ID_FEELY = 11;
	const SITE_ID_CURAZY = 10;
	const SITE_ID_CANDLE = 9;
	const SITE_ID_BUZZLIVE = 8;
	const SITE_ID_VIPBLOG = 7;
	const SITE_ID_NINEPOST = 6;
	const SITE_ID_SPORTSNEWS = 5;
	const SITE_ID_HOUKO = 4;
	const SITE_ID_NEWSOKU = 3;
	const SITE_ID_KOPIPE = 2;

	private function parseRow($siteId, $rssUrl){
		switch($siteId){
			case self::SITE_ID_INSYOKU:
			case self::SITE_ID_UWAKI:
			case self::SITE_ID_ENSOKU:
			case self::SITE_ID_ONIYOME:
			case self::SITE_ID_OGA:
			case self::SITE_ID_JIN:
			case self::SITE_ID_ORYOURI:
			case self::SITE_ID_KAOSU:
			case self::SITE_ID_KANASOKU:
			case self::SITE_ID_KARUKAN:
			case self::SITE_ID_KINISOKU:
			case self::SITE_ID_GURASOKU:
			case self::SITE_ID_GEINOUNEWS:
			case self::SITE_ID_GEINOU2CH:
			case self::SITE_ID_GOLDENTIMES:
			case self::SITE_ID_KOPIPEJOHOKYOKU:
			case self::SITE_ID_KABUZENRYOKU:
			case self::SITE_ID_SYURABA:
			case self::SITE_ID_SEIYUSOKUHOU:
			case self::SITE_ID_DIETSOKUHOU:
			case self::SITE_ID_DEJITALNEWS:
			case self::SITE_ID_TETUGAKU:
			case self::SITE_ID_TOKIHAKITA:
			case self::SITE_ID_NIKONIKO:
			case self::SITE_ID_EVESOKU:
			case self::SITE_ID_NW2:
			case self::SITE_ID_NEWS30OVER:
			case self::SITE_ID_NETA:
			case self::SITE_ID_NENSYU:
			case self::SITE_ID_HART:
			case self::SITE_ID_VIPSYOKUNIN:
			case self::SITE_ID_HACHIMA:
			case self::SITE_ID_HAMSTER:
			case self::SITE_ID_HIMAJIN:
			case self::SITE_ID_HIROIMONOTYUDOKU:
			case self::SITE_ID_BILOG:
			case self::SITE_ID_FESOKU:
			case self::SITE_ID_FRIDECHIKIN:
			case self::SITE_ID_BURABURA:
			case self::SITE_ID_PURISOKU:
			case self::SITE_ID_BURUSOKU:
			case self::SITE_ID_MATOMEIDE:
			case self::SITE_ID_MATOMESU:
			case self::SITE_ID_CHANNELPLUS:
			case self::SITE_ID_MATOMEBIJON:
			case self::SITE_ID_MUDAIDOCUMENT:
			case self::SITE_ID_MESIUMA:
			case self::SITE_ID_MOEOTA:
			case self::SITE_ID_MOMIAGE:
			case self::SITE_ID_YASESOKU:
			case self::SITE_ID_NISIKI:
			case self::SITE_ID_RAJIKKU:
			case self::SITE_ID_WARANOTE:
			case self::SITE_ID_IMOUTOHAVIPPER:
			case self::SITE_ID_ITASIN:
			case self::SITE_ID_ITAI:
			case self::SITE_ID_ANISOKUMAX:
			case self::SITE_ID_AJAJA:
			case self::SITE_ID_VIPWIDE:
			case self::SITE_ID_VIPORE:
			case self::SITE_ID_VIPSOKUHOU:
			case self::SITE_ID_ITSOKUHOU:
			case self::SITE_ID_ASOBU:
			case self::SITE_ID_FX2CH:
			case self::SITE_ID_VIPBLOG:
			case self::SITE_ID_FOOTBALLNET:
			case self::SITE_ID_SPORTSNEWS:
			case self::SITE_ID_HOUKO:
			case self::SITE_ID_NEWSOKU:
			case self::SITE_ID_KOPIPE:
			case self::SITE_ID_BYOKAN:
			case self::SITE_ID_ITSOKUHOU:
				$this->baseType1($rssUrl, $siteId);
				break;
			case self::SITE_ID_IMOUTOHAVIPPER:
				$this->parseImoutohavipper($rssUrl);
				break;
			case self::SITE_ID_ITASIN:
				$this->parseItasin($rssUrl);
				break;
			case self::SITE_ID_ITAI:
				$this->parseItai($rssUrl);
				break;
			case self::SITE_ID_ANISOKUMAX:
				$this->parseAnisokumax($rssUrl);
				break;
			case self::SITE_ID_AJAJA:
				$this->parseAjaja($rssUrl);
				break;
			case self::SITE_ID_WHATS:
				$this->parseWhats($rssUrl);
				break;
			case self::SITE_ID_VIPWIDE:
				$this->parseVipwide($rssUrl);
				break;
			case self::SITE_ID_VIPORE:
				$this->parseVipore($rssUrl);
				break;
			case self::SITE_ID_VIPSOKUHOU:
				$this->parseVipsokuhou($rssUrl);
				break;
			case self::SITE_ID_UPLOAD:
				$this->parseUpload($rssUrl);
				break;
			case self::SITE_ID_TEMITA:
				$this->parseTemita($rssUrl);
				break;
			case self::SITE_ID_POKETTI:
				$this->parsePoketti($rssUrl);
				break;
			case self::SITE_ID_OTODASU:
				$this->parseOtodasu($rssUrl);
				break;
			case self::SITE_ID_ITSOKUHOU:
				$this->parseItsokuhou($rssUrl);
				break;
			case self::SITE_ID_ASOBU:
				$this->parseAsobu($rssUrl);
				break;
			case self::SITE_ID_FX2CH:
				$this->parseFx2ch($rssUrl);
				break;
			case self::SITE_ID_FOOTBALLNET:
				$this->parseFootballnet($rssUrl);
				break;
			case self::SITE_ID_FEELY:
				$this->parseFeely($rssUrl);
				break;
			case self::SITE_ID_CURAZY:
				$this->parseCurazy($rssUrl);
				break;
			case self::SITE_ID_CANDLE:
				$this->parseCandle($rssUrl);
				break;
			case self::SITE_ID_BUZZLIVE:
				$this->parseBuzzLive($rssUrl);
				break;
			case self::SITE_ID_VIPBLOG:
				$this->parseVipblog($rssUrl);
				break;
			case self::SITE_ID_NINEPOST:
				$this->parseNinepost($rssUrl);
				break;
			case self::SITE_ID_SPORTSNEWS:
				$this->parseSports($rssUrl);
				break;
			case self::SITE_ID_HOUKO:
				$this->parseHouko($rssUrl);
				break;
			case self::SITE_ID_NEWSOKU:
				$this->parseNewsoku($rssUrl);
				break;
			case self::SITE_ID_BYOKAN:
				$this->baseType1($rssUrl, $siteId);
				break;
			case self::SITE_ID_GIZMODE:
				$this->parseGizmode($rssUrl);
				break;
			case self::SITE_ID_GIGAZINE:
				$this->parseGigazine($rssUrl);
				break;
			case self::SITE_ID_CNET:
				$this->parseCnet($rssUrl);
				break;
			case self::SITE_ID_LIFEHACKER:
				$this->parseLifehacker($rssUrl);
				break;
			case self::SITE_ID_OMOKORO:
				$this->parseOmokoro($rssUrl);
				break;
		}
	}

	private function parseOmokoro($rssUrl){
		$xml = simplexml_load_file($rssUrl);
		$itemList = $xml->channel->item;
		$stm = $this->createStatement();
		$stm->bindValue(':site_id', self::SITE_ID_OMOKORO);
		foreach ($itemList as $item) {
			// type-image
			$src = "";
			$html = str_get_html($item->description);
			$img = $html->find('img', 0);
			if(!empty($img)){
				$src = $img->src;
			}
			if($html)
			$html->clear();

			$publishedAt = strtotime($item->pubDate);
			$stm->bindValue(':title', $item->title);
			$stm->bindValue(':description', $item->description);
			$stm->bindValue(':link_url', $item->link);
			$stm->bindValue(':image_url', $src);
			$stm->bindValue(':published_at', $publishedAt);
			$stm->execute();
		}
	}

	private function parseLifehacker($rssUrl){
		$xml = simplexml_load_file($rssUrl);
		$itemList = $xml->channel->item;
		$stm = $this->createStatement();
		$stm->bindValue(':site_id', self::SITE_ID_LIFEHACKER);
		foreach ($itemList as $item) {
			// type-image
			$src = "";
			$html = str_get_html($item->description);
			$img = $html->find('img', 0);
			if(!empty($img)){
				$src = $img->src;
			}
			if($html)
			$html->clear();

			$publishedAt = strtotime($item->pubDate);
			$stm->bindValue(':title', $item->title);
			$stm->bindValue(':description', $item->description);
			$stm->bindValue(':link_url', $item->link);
			$stm->bindValue(':image_url', $src);
			$stm->bindValue(':published_at', $publishedAt);
			$stm->execute();
		}
	}

	private function parseCnet($rssUrl){
		$xml = simplexml_load_file($rssUrl);
		$itemList = $xml->item;
		$stm = $this->createStatement();
		$stm->bindValue(':site_id', self::SITE_ID_CNET);
		foreach ($itemList as $item) {
			// type-image
			$src = "";
			$html = str_get_html($item->description);
			$img = $html->find('img', 0);
			if(!empty($img)){
				$src = $img->src;
			}
			if($html)
			$html->clear();

			$publishedAt = strtotime($item->children("dc", true)->date);
			$stm->bindValue(':title', $item->title);
			$stm->bindValue(':description', $item->description);
			$stm->bindValue(':link_url', $item->link);
			$stm->bindValue(':image_url', $src);
			$stm->bindValue(':published_at', $publishedAt);
			$stm->execute();
		}
	}

	private function parseGigazine($rssUrl){
		$xml = simplexml_load_file($rssUrl);
		$itemList = $xml->channel->item;
		$stm = $this->createStatement();
		$stm->bindValue(':site_id', self::SITE_ID_GIGAZINE);
		foreach ($itemList as $item) {
			// type-image
			$src = "";
			$html = str_get_html($item->description);
			if($html)
			$html->clear();

			$publishedAt = strtotime($item->children("dc", true)->date);
			$stm->bindValue(':title', $item->title);
			$stm->bindValue(':description', $item->description);
			$stm->bindValue(':link_url', $item->link);
			$stm->bindValue(':image_url', $src);
			$stm->bindValue(':published_at', $publishedAt);
			$stm->execute();
		}
	}

	private function parseGizmode($rssUrl){
		$xml = simplexml_load_file($rssUrl);
		$itemList = $xml->channel->item;
		$stm = $this->createStatement();
		$stm->bindValue(':site_id', self::SITE_ID_GIZMODE);
		foreach ($itemList as $item) {
			// type-image
			$src = "";
			$html = str_get_html($item->description);
			$img = $html->find('img', 0);
			if(!empty($img)){
				$src = $img->src;
			}
			if($html)
			$html->clear();

			$publishedAt = strtotime($item->pubDate);
			$stm->bindValue(':title', $item->title);
			$stm->bindValue(':description', $item->description);
			$stm->bindValue(':link_url', $item->link);
			$stm->bindValue(':image_url', $src);
			$stm->bindValue(':published_at', $publishedAt);
			$stm->execute();
		}
	}

	private function parseWhats($rssUrl){
		$xml = simplexml_load_file($rssUrl);
		$itemList = $xml->channel->item;
		$stm = $this->createStatement();
		$stm->bindValue(':site_id', self::SITE_ID_WHATS);
		foreach ($itemList as $item) {

			// type-image
			$src = "";
			$html = file_get_html($item->link);
			$img = $html->find('div.type-image img', 0);
			if(!empty($img)){
				$src = $img->src;
			}
			if($html)
			$html->clear();

			$publishedAt = strtotime($item->pubDate);
			$stm->bindValue(':title', $item->title);
			$stm->bindValue(':description', $item->description);
			$stm->bindValue(':link_url', $item->link);
			$stm->bindValue(':image_url', $src);
			$stm->bindValue(':published_at', $publishedAt);
			$stm->execute();
		}
	}

	private function parseUpload($rssUrl){
		$xml = simplexml_load_file($rssUrl);
		$html = file_get_html("http://uploadmag.com/");
		$itemList = $xml->channel->item;

		$stm = $this->createStatement();
		$stm->bindValue(':site_id', self::SITE_ID_UPLOAD);

		foreach($itemList as $item){
			$imageUrl = "";
			$id = str_replace("http://uploadmag.com/?p=", "", $item->guid);

			$imgSrc = "";
			$img = $html->find(sprintf(".post-%d img", $id), 0);
			if($img){
				$imgSrc = $img->getAttribute("data-src");
			}

			$categories = array();
			foreach($item->category as $category){
				$categories[] = $category. "";
			}

			$publishedAt = strtotime($item->pubDate);
			$stm->bindValue(':title', $item->title);
			$stm->bindValue(':description', $item->description);
			$stm->bindValue(':link_url', $item->link);
			$stm->bindValue(':image_url', $imgSrc);
			$stm->bindValue(':published_at', $publishedAt);
			$stm->execute();
		}

		if($html)
		$html->clear();
	}

	private function parseTemita($rssUrl){
		$xml = simplexml_load_file($rssUrl);
		$html = file_get_html("http://temita.jp/");
		$itemList = $xml->channel->item;

		$stm = $this->createStatement();
		$stm->bindValue(':site_id', self::SITE_ID_TEMITA);

		foreach($itemList as $item){
			$imageUrl = "";
			$id = str_replace("http://temita.jp/?p=", "", $item->guid);

			$imgSrc = "";
			$img = $html->find(sprintf("#post-%d img", $id), 0);
			if($img){
				$imgSrc = $img->getAttribute("data-lazy-src");
			}

			$categories = array();
			foreach($item->category as $category){
				$categories[] = $category. "";
			}

			$publishedAt = strtotime($item->pubDate);
			$stm->bindValue(':title', $item->title);
			$stm->bindValue(':description', $item->description);
			$stm->bindValue(':link_url', $item->link);
			$stm->bindValue(':image_url', $imgSrc);
			$stm->bindValue(':published_at', $publishedAt);
			$stm->execute();
		}
		if($html)
			$html->clear();
	}

	private function parsePoketti($rssUrl){
		$xml = $xml = simplexml_load_file($rssUrl);
		$itemList = $xml->channel->item;

		$stm = $this->createStatement();
		$stm->bindValue(':site_id', self::SITE_ID_POKETTI);

		$html = file_get_html("http://pocketti.me/");
		foreach($itemList as $item){
			$id = str_replace("http://pocketti.me/?p=", "", $item->link);
			$selector = sprintf("#post-%s img", $id);
			$img = $html->find($selector, 0);
			$src = "";
			if(!empty($img)){
				$src = $img->src;
			}

			$publishedAt = strtotime($item->pubDate);
			$stm->bindValue(':title', $item->title);
			$stm->bindValue(':description', $item->description);
			$stm->bindValue(':link_url', $item->link);
			$stm->bindValue(':image_url', $src);
			$stm->bindValue(':published_at', $publishedAt);
			$stm->execute();
		}
		if($html)
			$html->clear();
	}

	private function parseOtodasu($rssUrl){
		$xml = $xml = simplexml_load_file($rssUrl);
		$itemList = $xml->channel->item;

		$stm = $this->createStatement();
		$stm->bindValue(':site_id', self::SITE_ID_OTODASU);

		foreach($itemList as $item){
			$publishedAt = strtotime($item->pubDate);
			$stm->bindValue(':title', $item->title);
			$stm->bindValue(':description', $item->description);
			$stm->bindValue(':link_url', $item->link);
			$stm->bindValue(':image_url', "");
			$stm->bindValue(':published_at', $publishedAt);
			$stm->execute();
		}

	}

	private function parseFeely($rssUrl){
		$xml = $xml = simplexml_load_file($rssUrl);
		$itemList = $xml->channel->item;

		$stm = $this->createStatement();
		$stm->bindValue(':site_id', self::SITE_ID_FEELY);

		foreach($itemList as $item){
			$html = file_get_html($item->link);
			$imgList = $html->find('#content_text img');
			$src = "";
			foreach($imgList as $img){
				$src = $img->getAttribute("data-lazy-src");
				if(!empty($src)){
					break;
				}
			}
			if($html)
			$html->clear();

			$publishedAt = strtotime($item->pubDate);
			$stm->bindValue(':title', $item->title);
			$stm->bindValue(':description', $item->description);
			$stm->bindValue(':link_url', $item->link);
			$stm->bindValue(':image_url', $src);
			$stm->bindValue(':published_at', $publishedAt);
			$stm->execute();

		}

	}

	private function parseCurazy($rssUrl){
		$xml = $xml = simplexml_load_file($rssUrl);
		$itemList = $xml->channel->item;

		$stm = $this->createStatement();
		$stm->bindValue(':site_id', self::SITE_ID_CURAZY);

		foreach($itemList as $item){
			$html = file_get_html($item->link);
			$imgList = $html->find('#content p img');
			$src = "";
			foreach($imgList as $img){
				$src = $img->getAttribute("data-lazy-src");
				if(!empty($src)){
					break;
				}
			}
			if($html)
			$html->clear();

			$publishedAt = strtotime($item->pubDate);
			$stm->bindValue(':title', $item->title);
			$stm->bindValue(':description', $item->description);
			$stm->bindValue(':link_url', $item->link);
			$stm->bindValue(':image_url', $src);
			$stm->bindValue(':published_at', $publishedAt);
			$stm->execute();
		}
	}

	private function parseCandle($rssUrl){
		$xml = simplexml_load_file($rssUrl);
		$itemList = $xml->channel->item;

		$stm = $this->createStatement();
		$stm->bindValue(':site_id', self::SITE_ID_CANDLE);

		foreach($itemList as $item){
			$imgSrc = "";
			$content = $item->children("content", true)->encoded;
			$html = str_get_html($content);
			$img = $html->find('img', 0);
			if($img){
				$imgSrc = $img->src;
			}

			$publishedAt = strtotime($item->pubDate);
			$stm->bindValue(":title", $item->title);
			$stm->bindValue(':description', strip_tags($content));
			$stm->bindValue(':link_url', $item->link);
			$stm->bindValue(':image_url', $imgSrc);
			$stm->bindValue(':published_at', $publishedAt);
			$stm->execute();

		}
	}

	private function parseBuzzLive($rssUrl){
		$xml = simplexml_load_file($rssUrl);
		$itemList = $xml->channel->item;
		$html = file_get_html("http://buzzlive.info/");

		$stm = $this->createStatement();
		$stm->bindValue(':site_id', self::SITE_ID_BUZZLIVE);

		foreach($itemList as $item){
			$imgSrc = "";
			$id = str_replace("http://buzzlive.info/?p=", "", $item->guid);
			$img = $html->find(sprintf("#post-%d img", $id), 0);
			if($img){
				$imgSrc = $img->src;
			}

			$publishedAt = strtotime($item->pubDate);
			$stm->bindValue(':title', $item->title);
			$stm->bindValue(':description', strip_tags($item->children("content", true)->encoded));
			$stm->bindValue(':link_url', $item->link);
			$stm->bindValue(':image_url', $imgSrc);
			$stm->bindValue(':published_at', $publishedAt);
			$stm->execute();
		}
		if($html)
		$html->clear();

	}

	private function parseNinepost($rssUrl){

		$stm = $this->createStatement();
		$stm->bindValue(':site_id', self::SITE_ID_NINEPOST);

		$html = file_get_html("http://9post.jp/");
		$liList = $html->find('.recommend li');

		$xml = simplexml_load_file($rssUrl); 
		$itemList = $xml->channel->item;

		foreach($itemList as $item){
			$imageUrl = "";
			$id = str_replace("http://9post.jp/", "", $item->link);

			foreach($liList as $li){
				$a = $li->find("a", 1);
				if(strstr($a->href, $id)){
					$img = $a->find('img', 0);
					$imageUrl = $img->src;
					break;
				}				
			}

			$publishedAt = strtotime($item->pubDate);
			$stm->bindValue(':title', $item->title);
			$stm->bindValue(':description', $item->description);
			$stm->bindValue(':link_url', $item->link);
			$stm->bindValue(':image_url', $imageUrl);
			$stm->bindValue(':published_at', $publishedAt);
			$stm->execute();
		}

		if($html)
		$html->clear();
	}

	private function baseType1($rssUrl, $siteId){
		$xml = simplexml_load_file($rssUrl); 
		$itemList = $xml->item;

		$stm = $this->createStatement();
		$stm->bindValue(':site_id', $siteId);
		foreach ($itemList as $item) {
			$publishedAt = strtotime($item->children("dc", true)->date);
			$contentHtml = $item->children("content", true)->encoded;
			$html = str_get_html($contentHtml);

			$img = $html->find('img.pict', 0);
			$imageSrc = "";
			if($img){
				$imageSrc = $img->src;
			}

			if($html)
			$html->clear();

			$stm->bindValue(':title', $item->title);
			$stm->bindValue(':description', $item->description);
			$stm->bindValue(':link_url', $item->link);
			$stm->bindValue(':image_url', $imageSrc);
			$stm->bindValue(':published_at', $publishedAt);
			$stm->execute();
		}
	}

	private function siteList(){
		$sql = "SELECT * FROM site ORDER BY id ASC";
		$list = $this->getSqlResult($sql);

		return $list;
	}

	private function createStatement(){
		$sql = "INSERT IGNORE INTO article "
		." (site_id, title, description, link_url, image_url, published_at, created_at) "
		. " VALUES "
		. " (:site_id, :title, :description, :link_url, :image_url, :published_at, :created_at)";
		$stm = $this->prepare($sql);
		$stm->bindValue(':created_at', time());

		return $stm;
	}

}

$p = new Parser(DB_HOST, DB_NAME, DB_USER, DB_PASSWORD);
