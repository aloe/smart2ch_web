<?php

require_once("config.php");
require_once("Base.php");


class Api extends Base{

	public function apiData(){
		$sql = "SELECT * FROM site ORDER BY id DESC ";
		$siteList = $this->getSqlResult($sql);

		$sql = "SELECT * FROM article ORDER BY published_at DESC LIMIT 7000";
		$articleList = $this->getSqlResult($sql);

		return array(
			'site'=>$siteList
			, 'article'=>$articleList
		);
	}

	public function apiDataNoJa(){
		$sql = "SELECT * FROM site WHERE use_review = 1 ORDER BY priority DESC";
		$siteList = $this->getSqlResult($sql);

		$sql = "SELECT id, site_id, title, link_url, image_url FROM article WHERE (SELECT use_review FROM site WHERE id = article.site_id) = 1 ORDER BY published_at DESC LIMIT 5000";
		$articleList = $this->getSqlResult($sql);

		return array(
			'site'=>$siteList
			, 'article'=>$articleList
		);
	}

	public function registerToken(){
		if(empty($_POST['devicetoken'])) die("invalid request");

		$sql = "INSERT IGNORE INTO device (device_token, created_at) VALUES (:device_token, :created_at)";
		$stm = $this->prepare($sql);
		$stm->bindValue(':device_token', $_POST['devicetoken']);
		$stm->bindValue(':created_at', time());
		$stm->execute();
	}

	public function ihan(){
		if(empty($_GET['url'])) die("invalid request");

		$sql = "INSERT violation (url, created_at) VALUES (:url, :created_at)";
		$stm = $this->prepare($sql);
		$stm->bindValue(':url', $_GET['url']);
		$stm->bindValue(':created_at', time());
		$stm->execute();

	}

}

$api = new Api(DB_HOST, DB_NAME, DB_USER, DB_PASSWORD);

