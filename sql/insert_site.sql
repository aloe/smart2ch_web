-- phpMyAdmin SQL Dump
-- version 4.2.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: 2014 年 9 月 13 日 12:16
-- サーバのバージョン： 5.5.38
-- PHP Version: 5.5.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `smart2ch`
--

--
-- テーブルのデータのダンプ `site`
--

INSERT INTO `site` (`id`, `name`, `url`, `rss_url`, `priority`, `created_at`, `updated_at`, `deleted`, `use_review`) VALUES
(3, 'ニュー速クオリティ', 'http://news4vip.livedoor.biz/', 'http://news4vip.livedoor.biz/index.rdf', 186, 1407128570, 0, 0, 0),
(5, '２ｃｈスポーツニュースナビ', 'http://2chsportsnavi.blog.fc2.com/', 'http://2chsportsnavi.blog.fc2.com/?xml', 178, 1407128648, 0, 0, 0),
(6, '9ポスト', 'http://9post.jp/', 'http://9post.jp/feed', 0, 1407128708, 1, 0, 1),
(8, 'Buzzlive', 'http://buzzlive.info/', 'http://buzzlive.info/feed/', 0, 1407128788, 1, 0, 1),
(10, 'curazy', 'http://curazy.com/', 'http://curazy.com/feed', 0, 1407128878, 1, 0, 1),
(11, 'feely', 'http://feely.jp/', 'http://feely.jp/feed/', 0, 1407128911, 1, 0, 1),
(14, 'iPhoneで遊ぶ夫', 'http://www.asobuoiphone.com/', 'http://www.asobuoiphone.com/index.rdf', 177, 1407129027, 0, 0, 0),
(16, 'otodas（オトダス）　～鳥肌モノ音楽動画～', 'http://otodas.com/', 'http://otodas.com/feed', 0, 1407129104, 1, 0, 1),
(18, 'TEMITA', 'http://temita.jp/', 'http://temita.jp/feed/', 0, 1407142140, 1, 0, 1),
(19, 'UPLOAD', 'http://uploadmag.com/', 'http://uploadmag.com/feed', 0, 1407142187, 1, 0, 1),
(20, 'VIPPER速報', 'http://vippers.jp/', 'http://vippers.jp/index.rdf', 187, 1407142231, 0, 0, 0),
(21, 'VIPPERな俺', 'http://blog.livedoor.jp/news23vip/', 'http://blog.livedoor.jp/news23vip/index.rdf', 176, 1407142304, 0, 0, 0),
(22, 'VIPワイドガイド', 'http://news4wide.livedoor.biz/', 'http://news4wide.livedoor.biz/index.rdf', 175, 1407142341, 0, 0, 0),
(23, 'whats', 'http://whats.be/', 'http://whats.be/feed.rss', 0, 1407142374, 0, 0, 1),
(24, 'あじゃじゃしたー', 'http://blog.livedoor.jp/chihhylove/', 'http://blog.livedoor.jp/chihhylove/index.rdf', 188, 1407142418, 0, 0, 0),
(25, 'アニ速MAX', 'http://maxinformation.blog.jp/', 'http://maxinformation.blog.jp/index.rdf', 174, 1407142452, 0, 0, 0),
(28, '痛いニュース(ﾉ∀`)', 'http://blog.livedoor.jp/dqnplus/', 'http://blog.livedoor.jp/dqnplus/index.rdf', 201, 1407142566, 0, 0, 0),
(29, 'いたいしん！', 'http://itaishinja.com/', 'http://itaishinja.com/index.rdf', 173, 1407142618, 0, 0, 0),
(30, '妹はVIPPER', 'http://vipsister23.com/', 'http://vipsister23.com/index.rdf', 172, 1407142687, 0, 0, 0),
(31, '飲食速報(ﾟдﾟ)ｳﾏ-', 'http://insyoku.livedoor.biz/', 'http://insyoku.livedoor.biz/index.rdf', 171, 1407142722, 0, 0, 0),
(32, '浮気ちゃんねる', 'http://uwakich.com/', 'http://uwakich.com/index.rdf', 189, 1407142756, 0, 0, 0),
(33, '円速 株やFXなど投資系2chまとめ', 'http://kabooo.net/', 'http://kabooo.net/index.rdf', 169, 1407142821, 0, 0, 0),
(35, '鬼嫁ちゃんねる', 'http://oniyomech.livedoor.biz/', 'http://oniyomech.livedoor.biz/index.rdf', 189, 1407142896, 0, 0, 0),
(36, 'オーガch.-パズドラ攻略まとめ速報', 'http://h-pon.doorblog.jp/', 'http://h-pon.doorblog.jp/index.rdf', 168, 1407142929, 0, 0, 0),
(37, 'オレ的ゲーム速報＠刃', 'http://jin115.com/', 'http://jin115.com/index.rdf', 190, 1407142976, 0, 0, 0),
(39, 'お料理速報', 'http://oryouri.2chblog.jp/', 'http://oryouri.2chblog.jp/index.rdf', 167, 1407143048, 1, 0, 0),
(40, 'カオスちゃんねる', 'http://chaos2ch.com/', 'http://chaos2ch.com/index.rdf', 191, 1407143080, 0, 0, 0),
(41, 'カナ速', 'http://kanasoku.info/', 'http://kanasoku.info/index.rdf', 192, 1407143223, 0, 0, 0),
(42, 'かるかんタイムズ', 'http://karukantimes.com/', 'http://karukantimes.com/index.rdf', 10, 1407143281, 1, 0, 0),
(43, 'キニ速', 'http://blog.livedoor.jp/kinisoku/', 'http://blog.livedoor.jp/kinisoku/index.rdf', 193, 1407143320, 0, 0, 0),
(44, 'ぐら速 -声優まとめ-', 'http://grasoku.blomaga.jp/', 'http://grasoku.blomaga.jp/index.rdf', 165, 1407143356, 0, 0, 0),
(46, '芸能ニュース２ch報道', 'http://geinou2news.blog.fc2.com/', 'http://geinou2news.blog.fc2.com/?xml', 164, 1407143437, 0, 0, 0),
(47, '芸能人の気になる噂', 'http://blog.livedoor.jp/uwasainfo/', 'http://blog.livedoor.jp/uwasainfo/index.rdf', 163, 1407143475, 0, 0, 0),
(48, 'ゴールデンタイムズ', 'http://blog.livedoor.jp/goldennews/', 'http://blog.livedoor.jp/goldennews/index.rdf', 194, 1407143543, 0, 0, 0),
(49, 'コピペ情報局', 'http://news.2chblog.jp/', 'http://news.2chblog.jp/index.rdf', 195, 1407143582, 0, 0, 0),
(51, '市況かぶ全力２階建', 'http://kabumatome.doorblog.jp/', 'http://kabumatome.doorblog.jp/index.rdf', 162, 1407143665, 1, 0, 0),
(52, '修羅場ハザード', 'http://syurabahazard.com/', 'http://syurabahazard.com/index.rdf', 161, 1407144067, 0, 0, 0),
(53, '声優☆速報', 'http://seiyuusokuhou.blog106.fc2.com/', 'http://seiyuusokuhou.blog106.fc2.com/?xml', 160, 1407144103, 0, 0, 0),
(54, 'ダイエット速報', 'http://blog.livedoor.jp/diet2channel/', 'http://blog.livedoor.jp/diet2channel/index.rdf', 159, 1407144203, 0, 0, 0),
(55, 'デジタルニューススレッド', 'http://digital-thread.com/', 'http://digital-thread.com/index.rdf', 158, 1407144237, 0, 0, 0),
(56, '哲学ニュースnwk', 'http://blog.livedoor.jp/nwknews/', 'http://blog.livedoor.jp/nwknews/index.rdf', 196, 1407144278, 0, 0, 0),
(57, '時は来た！それだけだ', 'http://tokihakita.blog91.fc2.com/', 'http://tokihakita.blog91.fc2.com/?xml', 157, 1407144318, 0, 0, 0),
(58, 'ニコニコVIP2ch', 'http://blog.livedoor.jp/nicovip2ch/', 'http://blog.livedoor.jp/nicovip2ch/index.rdf', 156, 1407477634, 0, 0, 0),
(59, '二ちゃんまとめ-EVE速-', 'http://blog.livedoor.jp/luckysoku/', 'http://blog.livedoor.jp/luckysoku/index.rdf', 155, 1407477668, 0, 0, 0),
(60, 'ニュースウォッチ２ちゃんねる -nw2-', 'http://nw2.blog112.fc2.com/', 'http://nw2.blog112.fc2.com/?xml', 154, 1407477731, 0, 0, 0),
(61, 'ニュース30over', 'http://www.news30over.com/', 'http://www.news30over.com/index.rdf', 153, 1407477765, 0, 0, 0),
(62, 'ねたAtoZ', 'http://netaatoz.jp/', 'http://netaatoz.jp/index.rdf', 197, 1407477797, 0, 0, 0),
(63, '年収・お給料速報', 'http://nenshusokuho.doorblog.jp/', 'http://nenshusokuho.doorblog.jp/index.rdf', 152, 1407477830, 0, 0, 0),
(64, 'はーとログ', 'http://blog.livedoor.jp/love120331/', 'http://blog.livedoor.jp/love120331/index.rdf', 189, 1407477868, 0, 0, 0),
(65, '働くモノニュース : 人生VIP職人ブログwww', 'http://workingnews.blog117.fc2.com/', 'http://workingnews.blog117.fc2.com/?xml', 150, 1407477891, 0, 0, 0),
(66, 'はちま起稿', 'http://blog.esuteru.com/', 'http://blog.esuteru.com/index.rdf', 198, 1407477923, 0, 0, 0),
(67, 'ハムスター速報', 'http://hamusoku.com/', 'http://hamusoku.com/index.rdf', 199, 1407477953, 0, 0, 0),
(68, '暇人＼(^o^)／速報', 'http://himasoku.com/', 'http://himasoku.com/index.rdf', 200, 1407477988, 0, 0, 0),
(69, 'ヒロイモノ中毒', 'http://cherio199.blog120.fc2.com/', 'http://cherio199.blog120.fc2.com/?xml', 149, 1407478021, 0, 0, 0),
(70, '美ログ@2ch', 'http://birogu2ch.ldblog.jp/', 'http://birogu2ch.ldblog.jp/index.rdf', 189, 1407478060, 0, 0, 0),
(71, 'ふぇー速', 'http://fesoku.net/', 'http://fesoku.net/index.rdf', 147, 1407478094, 0, 0, 0),
(72, 'フライドチキンは空をとぶ -フラソラ-', 'http://morinogorira.seesaa.net/', 'http://morinogorira.seesaa.net/index.rdf', 146, 1407478127, 0, 0, 0),
(73, 'ブラブラブラウジング', 'http://brow2ing.doorblog.jp/', 'http://brow2ing.doorblog.jp/index.rdf', 145, 1407478161, 0, 0, 0),
(74, 'ぷりそく！', 'http://purisoku.com/', 'http://purisoku.com/index.rdf', 140, 1407478220, 0, 0, 0),
(75, 'ぶる速-VIP', 'http://burusoku-vip.com/', 'http://burusoku-vip.com/index.rdf', 141, 1407478256, 0, 0, 0),
(76, 'まとめいど', 'http://matomade.2chblog.jp/', 'http://matomade.2chblog.jp/index.rdf', 142, 1407478291, 0, 0, 0),
(77, 'まとめす', 'http://matomesu.com/', 'http://matomesu.com/index.rdf', 143, 1407478399, 0, 0, 0),
(78, 'まとめニュースちゃんねるぷらす', 'http://blog.livedoor.jp/ainbekker-news2/', 'http://blog.livedoor.jp/ainbekker-news2/index.rdf', 179, 1407478436, 0, 0, 0),
(79, 'まとめびじょん', 'http://www.matome-vision.com/', 'http://www.matome-vision.com/index.rdf', 180, 1407478469, 0, 0, 0),
(80, '無題のドキュメント', 'http://www.mudainodocument.com/', 'http://www.mudainodocument.com/index.rdf', 181, 1407478516, 0, 0, 0),
(81, 'メシウマ速報', 'http://meshiuma.org/', 'http://meshiuma.org/index.rdf', 183, 1407478556, 0, 0, 0),
(82, '萌えオタニュース速報', 'http://otanews.livedoor.biz/', 'http://otanews.livedoor.biz/index.rdf', 182, 1407478592, 0, 0, 0),
(83, 'もみあげチャ～シュ～', 'http://michaelsan.livedoor.biz/', 'http://michaelsan.livedoor.biz/index.rdf', 184, 1407478634, 0, 0, 0),
(84, 'やせ速！', 'http://yasesoku.2chblog.jp/', 'http://yasesoku.2chblog.jp/index.rdf', 139, 1407479084, 0, 0, 0),
(85, 'ライフハックちゃんねる弐式', 'http://lifehack2ch.livedoor.biz/', 'http://lifehack2ch.livedoor.biz/index.rdf', 185, 1407479120, 0, 0, 0),
(86, 'ラジック', 'http://rajic.2chblog.jp/', 'http://rajic.2chblog.jp/index.rdf', 138, 1407479151, 0, 0, 0),
(87, 'ワラノート', 'http://waranote.livedoor.biz/', 'http://waranote.livedoor.biz/index.rdf', 137, 1407479184, 0, 0, 0),
(88, '秒刊SUNDAY', 'http://www.yukawanet.com/', 'http://www.yukawanet.com/index.rdf', 0, 1409182981, 0, 0, 0),
(89, 'ギズモード・ジャパン', 'http://www.gizmodo.jp/', 'http://feeds.gizmodo.jp/rss/gizmodo/index.xml?rss', 0, 1409183077, 0, 0, 1),
(90, 'GIGAZINE', 'http://gigazine.net/', 'http://feed.rssad.jp/rss/gigazine/rss_2.0', 0, 1409183133, 0, 0, 1),
(91, 'CNET Japan', 'http://japan.cnet.com', 'http://feeds.japan.cnet.com/rss/cnet/all.rdf', 0, 1409183203, 0, 0, 1),
(92, 'ライフハッカー', 'http://www.lifehacker.jp/', 'http://feeds.lifehacker.jp/rss/lifehacker/index.xml?rss', 0, 1409183245, 0, 0, 1),
(93, 'オモコロ', 'http://omocoro.jp/', 'http://omocoro.jp/feed', 0, 1409183428, 0, 0, 0),
(94, 'IT速報', 'http://blog.livedoor.jp/itsoku/', 'http://blog.livedoor.jp/itsoku/index.rdf', 100, 1409185837, 0, 0, 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
